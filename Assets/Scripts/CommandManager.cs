﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandManager : MonoBehaviour {

	public Command [] commands;

	private List<Command> possibleCommands;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Command getRandomCommand(){
		possibleCommands = new List<Command>();
		//Debug.Log(GameManager.instance.CurrentState);
		for(int i=0; i<commands.Length; i++){
		//Debug.Log("check" +commands[i].name);
			if((commands[i].requiredState == GameManager.instance.ShouldBeState) || commands[i].requiredState == bodyState.Any){
				
				possibleCommands.Add(commands[i]);
				//Debug.Log("possible command: "+commands[i].name + " because "+commands[i].requiredState+" = " +GameManager.instance.CurrentState);
			}
		}
		int test = Mathf.FloorToInt(Random.Range(0, (float)possibleCommands.Count-.1f));
		return possibleCommands[test];

	}

	public Command getCommandFromKey(KeyPress k){
		foreach(Command c in commands){
			if(k.key==c.key && k.pressedDown != c.doOnKeyUp){
				return c;
			}
		}
		return commands[0];
	}
}
