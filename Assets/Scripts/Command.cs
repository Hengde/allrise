﻿using UnityEngine;
using System.Collections;

public class Command : MonoBehaviour {

	public string name;
	public KeyCode key;
	public bool doOnKeyUp = false;
	public bodyState requiredState = bodyState.Any;
	public AudioClip commandSound;
	public AudioClip crowdSound;
	public AudioClip playerSound;

	public GameObject playerSprite;
	public GameObject priestSprite;
	public GameObject crowdSprite;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}

public enum bodyState{Sitting, Standing, Kneeling, Any};
