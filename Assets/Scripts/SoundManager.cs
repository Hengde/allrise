﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource priestAudio;
	public AudioSource playerAudio;
	public AudioSource crowdAudio;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void playPriestSound(AudioClip a){
		priestAudio.clip = a;
		priestAudio.Play();
	}

	public void playPlayerSound(KeyPress kp){
		Command c = GameManager.instance.cm.getCommandFromKey(kp);
		playerAudio.clip = c.playerSound;
		playerAudio.Play();

	}

	public void playCrowdSound(AudioClip a){
		crowdAudio.clip = a;
		crowdAudio.Play();
	}
}
