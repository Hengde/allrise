﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public bool rotating = false;
	public bool isPlayerInHell;
	Quaternion start;


	void Start () {
		start = transform.rotation;
		StartShake();
	}
	// Update is called once per frame
	void Update () {
		if (rotating == true && !isPlayerInHell){
			transform.Rotate (0,0,.5f-Mathf.PingPong(3f*Time.time,1f));
		}
		if(isPlayerInHell){
			transform.Rotate(new Vector3(0,0,1f));
		}

	}

	public void StartShake () {
		rotating = true;
		//transform.localScale*=1.25f;
	}

	public void EndShake () {
		rotating = false;
		transform.rotation = start;
		//transform.localScale*=0.8f;
	}
}
