﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Space)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.Space, true));
			GameManager.instance.CurrentState = bodyState.Standing;
		}
		if(Input.GetKeyUp(KeyCode.Space)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.Space, false));
			GameManager.instance.CurrentState = bodyState.Sitting;
		}


		if(Input.GetKeyDown(KeyCode.LeftShift)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.LeftShift, true));
			GameManager.instance.CurrentState = bodyState.Kneeling;
		}
		if(Input.GetKeyUp(KeyCode.LeftShift)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.LeftShift, false));
			GameManager.instance.CurrentState = bodyState.Sitting;
		}

		if(Input.GetKeyDown(KeyCode.W)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.W, true));
		}
//		if(Input.GetKeyUp(KeyCode.W)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.W, false));
//		}

		if(Input.GetKeyDown(KeyCode.Q)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.Q, true));
		}
//		if(Input.GetKeyUp(KeyCode.Q)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.Q, false));
//		}

		if(Input.GetKeyDown(KeyCode.E)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.E, true));
		}
//		if(Input.GetKeyUp(KeyCode.E)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.E, false));
//		}

		if(Input.GetKeyDown(KeyCode.A)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.A, true));
		}
//		if(Input.GetKeyUp(KeyCode.A)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.A, false));
//		}

		if(Input.GetKeyDown(KeyCode.S)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.S, true));
		}
//		if(Input.GetKeyUp(KeyCode.S)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.S, false));
//		}

		if(Input.GetKeyDown(KeyCode.Z)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.Z, true));
		}
//		if(Input.GetKeyUp(KeyCode.Z)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.Z, false));
//		}

		if(Input.GetKeyDown(KeyCode.X)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.X, true));
		}
//		if(Input.GetKeyUp(KeyCode.X)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.X, false));
//		}

		if(Input.GetKeyDown(KeyCode.C)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.LeftShift, true));
		}
//		if(Input.GetKeyUp(KeyCode.C)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.LeftShift, false));
//		}

		if(Input.GetKeyDown(KeyCode.D)){
			GameManager.instance.receiveInput(new KeyPress(KeyCode.D, true));
		}
//		if(Input.GetKeyUp(KeyCode.D)){
//			GameManager.instance.receiveInput(new KeyPress(KeyCode.D, false));
//		}




	}
}

public class KeyPress{

	public KeyCode key;
	public bool pressedDown;

	public KeyPress (KeyCode k, bool p){
		key = k;
		pressedDown = p;
	}

}