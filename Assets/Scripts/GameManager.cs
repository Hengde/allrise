﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	private static GameManager _instance;
	public static GameManager instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<GameManager>();
				//				DontDestroyOnLoad(_instance.gameObject);
			}
			return _instance;
		}
	}

	public Slider pietyMeter;

	private TimeManager tm;
	private InputManager im;
	public CommandManager cm;

	public VisualsManager vm;
	public SoundManager sm;

	private bool acceptingInput;
	private float inputTime;
	private float minInputTime;
	private float inputCountdown;

	private KeyCode expectedKey;
	private bool expectUp;

	private bool didHit;

	private float currentPiety;
	public float maxPiety;
	public int pietyDownAmount;
	public int pietyUpAmount;

	public Command firstCommand;
	private bool firstTime;

	public RectTransform timeLeftBar;



	[HideInInspector]
	public bodyState CurrentState {get;set;}
	[HideInInspector]
	public bodyState ShouldBeState {get;set;}


	private Command currentCommand;


	// Use this for initialization
	void Start () {
		firstTime = true;
		CurrentState = bodyState.Sitting;
		ShouldBeState = bodyState.Sitting;
		tm = GetComponent<TimeManager>();
		im = GetComponent<InputManager>();
		inputTime = 1.5f;
		minInputTime = .65f;
		inputCountdown = inputTime;

		currentPiety = maxPiety;

		pietyMeter.maxValue = maxPiety;
		pietyMeter.value = currentPiety;
	}
	
	// Update is called once per frame
	void Update () {
		

		if(acceptingInput){
			inputCountdown -=Time.deltaTime;
			timeLeftBar.sizeDelta = new Vector2( (300*inputCountdown/inputTime), 17);
		} 
		if(inputCountdown<=0){
			checkIfHit();
			acceptingInput = false;
			inputCountdown = inputTime;
			didHit = false;
		}

		pietyMeter.value = currentPiety;

		if(currentPiety <=0){
			goToHell();
		}

	}

	public void doCommand(){

		//pick a command from Command Manager
		if(firstTime){
			firstTime = false;
			currentCommand = firstCommand;
		} else {
			currentCommand = cm.getRandomCommand();
		}
		if(currentCommand.name == "Stand"){
			ShouldBeState = bodyState.Standing;
		} else if (currentCommand.name == "Kneel") {
			ShouldBeState = bodyState.Kneeling;
		} else if (currentCommand.name == "SitFromStanding" || currentCommand.name == "SitFromKneeling"){
			ShouldBeState = bodyState.Sitting;
		}

		vm.movePriest(currentCommand);
		Invoke("moveCrowd", inputTime);
		sm.playPriestSound(currentCommand.commandSound);
		expectedKey = currentCommand.key;
		expectUp = currentCommand.doOnKeyUp;

		//send command to audio and visual managers
		Debug.Log(currentCommand.name + currentCommand.key.ToString());
		//accept player input
		acceptingInput = true;
	}

	public void receiveInput(KeyPress kp){

		//do sound and animation first no matter what
//		Debug.Log(kp.key.ToString() + " " + kp.pressedDown);
//		Debug.Log(acceptingInput + " " + inputCountdown);
		sm.playPlayerSound(kp);
		vm.movePlayer(kp);

		if(kp.key==KeyCode.Space && kp.pressedDown){
			CurrentState = bodyState.Standing;
		} else if (kp.key==KeyCode.Space && !kp.pressedDown){
			CurrentState = bodyState.Sitting;
		} else if (kp.key==KeyCode.LeftShift && !kp.pressedDown){
			CurrentState = bodyState.Sitting;
		} else if (kp.key==KeyCode.LeftShift && !kp.pressedDown){
			CurrentState = bodyState.Sitting;
		}

		if(acceptingInput){
			
			//if we sat, is it time to sit?
//			if(kp.key == KeyCode.Space || kp.key == KeyCode.LeftShift && !kp.pressedDown){
//				if(expectUp && expectedKey == kp.key){
//					goodHit();
//				} else {
//					badHit();
//				}
//			}

			didHit = true;
			if(kp.key == expectedKey && kp.pressedDown != expectUp){
				goodHit();
			} else {
				badHit();
			}


		} else {
			if(
				ShouldBeState==bodyState.Standing && kp.key==KeyCode.Space && kp.pressedDown ||
				ShouldBeState==bodyState.Kneeling && kp.key==KeyCode.LeftShift && kp.pressedDown ||
				ShouldBeState==bodyState.Sitting && kp.key==KeyCode.Space && !kp.pressedDown ||
				ShouldBeState==bodyState.Sitting && kp.key==KeyCode.LeftShift && !kp.pressedDown
			){

			} else {
				badHit();
			}
		}

	}

	private void moveCrowd(){
		vm.moveCrowd(currentCommand);
		sm.playCrowdSound(currentCommand.crowdSound);
	}

	private void goodHit(){

		decreaseInputTime();
		tm.decreaseTimeBetweenCommands();
		Debug.Log("GOOD");
		currentPiety += pietyUpAmount;
		if(currentPiety>maxPiety){
			currentPiety = maxPiety;
		}
	}

	private void badHit(){
		Debug.Log("BAD");
		currentPiety -= pietyDownAmount;

	}

	private void checkIfHit(){
		if(currentCommand.name=="Stand" && CurrentState==bodyState.Standing ||
			currentCommand.name=="Kneel" && CurrentState==bodyState.Kneeling ||
			currentCommand.name=="SitFromStanding" && CurrentState==bodyState.Sitting ||
			currentCommand.name=="Kneel" && CurrentState==bodyState.Sitting

		){

		}else {
			if(!didHit){
				badHit();
			}
		}
	}

	private void goToHell(){
		SceneManager.LoadScene("Hell");
	}

	private void decreaseInputTime(){
		if(inputTime > minInputTime){
			inputTime -=.05f;
		}
	}

	public float PietyLevel(){
		return currentPiety;
	}
}
