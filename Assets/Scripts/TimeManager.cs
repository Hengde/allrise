﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {

	private const float initialTimeBetweenCommands = 3f;
	private float timeBetweenCommands;
	private float minTimeBetweenCommands;
	private float countdownTimer;

	// Use this for initialization
	void Start () {
		timeBetweenCommands = initialTimeBetweenCommands;
		countdownTimer = timeBetweenCommands;
		minTimeBetweenCommands = 1.5f;
	}
	
	// Update is called once per frame
	void Update () {

		countdownTimer -= Time.deltaTime;

		if(countdownTimer <=0){
			GameManager.instance.doCommand();
			countdownTimer = timeBetweenCommands;
		}
	
	}

	public void decreaseTimeBetweenCommands(){
		if(timeBetweenCommands > minTimeBetweenCommands){
			timeBetweenCommands -=.1f;
			Debug.Log("Time btw commands: " + timeBetweenCommands);
		}
	}
}
