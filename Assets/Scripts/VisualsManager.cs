﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VisualsManager : MonoBehaviour {

	public SpriteRenderer hellFade;

	public GameObject bubble;
	public Text bubbleText;
	public GameObject activePriest;
	public GameObject activeCrowd;
	public GameObject activePlayer;

	public GameObject neutralPriest;
	public GameObject neutralCrowd;
	public GameObject neutralPlayer;

	public GameObject pSitNeutral;
	public GameObject pSitAction;
	public GameObject pStandNeutral;
	public GameObject pStandAction;
	public GameObject pStandAction2;
	public GameObject pKneelNeutral;
	public GameObject pKneelAction;

	public RectTransform timeLeftBar;

	private float priestResetTime;

	// Use this for initialization
	void Start () {
		priestResetTime = 1f;
		activePlayer = pSitNeutral;
	}
	
	// Update is called once per frame
	void Update () {
		updateHellColor();
	}

	public void movePriest(Command c){
		//activate bubble
		//Debug.Log(c.key);
		if(c.key == KeyCode.LeftShift){
			bubbleText.text = "SHIFT";
		} else {
			bubbleText.text = c.key.ToString().ToUpper();
		}

		//if c.key.toString
		bubble.gameObject.SetActive(true);
		Invoke("hideBubble",priestResetTime);


		//actually move priest
		activePriest.SetActive(false);
		c.priestSprite.SetActive(true);
		activePriest= c.priestSprite;
		Invoke("resetPriest",priestResetTime);


	}

	private void resetPriest(){
		//Debug.Log(activePriest);
		activePriest.SetActive(false);
		neutralPriest.SetActive(true);
		activePriest = neutralPriest;
	}

	public void movePlayer(KeyPress kp){

		if(kp.key==KeyCode.Space && kp.pressedDown){
			activePlayer.SetActive(false);
			pStandNeutral.SetActive(true);
			activePlayer = pStandNeutral;
		} else if(kp.key==KeyCode.Space && !kp.pressedDown){
			activePlayer.SetActive(false);
			pSitNeutral.SetActive(true);
			activePlayer = pSitNeutral;
		} else if(kp.key==KeyCode.LeftShift && kp.pressedDown){
			activePlayer.SetActive(false);
			pKneelNeutral.SetActive(true);
			activePlayer = pKneelNeutral;
		} else if(kp.key==KeyCode.LeftShift && !kp.pressedDown){
			activePlayer.SetActive(false);
			pSitNeutral.SetActive(true);
			activePlayer = pSitNeutral;
		} else {
			
			if(GameManager.instance.CurrentState == bodyState.Sitting){
				activePlayer.SetActive(false);
				pSitAction.SetActive(true);
				activePlayer = pSitAction;
				Invoke("resetChar",.25f);
			} else if(GameManager.instance.CurrentState == bodyState.Standing){
				activePlayer.SetActive(false);
				if(kp.key == KeyCode.W || kp.key == KeyCode.E){
					pStandAction2.SetActive(true);
					activePlayer = pStandAction;
				} else {
					pStandAction.SetActive(true);
					activePlayer = pStandAction;
				}
				Invoke("resetChar",.25f);
			} else if(GameManager.instance.CurrentState == bodyState.Kneeling){
				activePlayer.SetActive(false);
				pKneelAction.SetActive(true);
				activePlayer = pKneelAction;
				Invoke("resetChar",.25f);
			}

		}

	}

	public void resetChar(){
		if(GameManager.instance.CurrentState == bodyState.Sitting){
			activePlayer.SetActive(false);
			pSitNeutral.SetActive(true);
			activePlayer = pSitNeutral;
		} else if(GameManager.instance.CurrentState == bodyState.Standing){
			pStandAction.SetActive(false);
			pStandAction2.SetActive(false);
			pStandNeutral.SetActive(true);
			activePlayer = pStandNeutral;
		} else if(GameManager.instance.CurrentState == bodyState.Kneeling){
			activePlayer.SetActive(false);
			pKneelNeutral.SetActive(true);
			activePlayer = pKneelNeutral;
		}
	}

	public void moveCrowd(Command c){
		
		if(c.name=="Stand" || c.name=="Kneel"){
			Debug.Log("Stand or Kneel");
			activeCrowd.SetActive(false);
			c.crowdSprite.SetActive(true);
			activeCrowd = c.crowdSprite;
		} else if(c.name=="SitFromStanding" || c.name=="SitFromKneeling"){
			Debug.Log("Sit");
			resetCrowd();
		}

	}

	private void resetCrowd(){
		//Debug.Log(activeCrowd);
		activeCrowd.SetActive(false);
		activeCrowd = neutralCrowd;
		neutralCrowd.SetActive(true);
		//activeCrowd = neutralCrowd;
	}

	public void hideBubble(){
		bubble.gameObject.SetActive(false);
		timeLeftBar.sizeDelta = new Vector2(300,17);
	}

	public void setNeutralPlayer(){

	}

	public void updateHellColor(){
		
		float alpha = (GameManager.instance.pietyMeter.maxValue - GameManager.instance.PietyLevel())/GameManager.instance.pietyMeter.maxValue;
		hellFade.color = new Color(1f,0f,0f,alpha*.376f);
	}

}
